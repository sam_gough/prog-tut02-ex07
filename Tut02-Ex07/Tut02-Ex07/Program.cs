﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut02_Ex07
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            var counter = 20;

            Console.WriteLine("Please enter a number from 1 to 20.");
            i = int.Parse(Console.ReadLine());

            do
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");

                i++;
            } while (i < counter);
        }
    }
}
